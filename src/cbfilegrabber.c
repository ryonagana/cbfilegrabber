#include <stdio.h>
#include <stdlib.h>


#ifdef WIN32
	/* Sheeeeeeeeit */
	#error "NO WIN32 FOR YOU MUTHAFUCKA!"
#else 


#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#endif

#include "../include/mem.h"
#include "../include/con_color.h"


#define MAJOR_VER 0
#define MINOR_VER 0
#define BUILD_VER 2
#define BUILD_PREFIX "beta"

#define EXTENSION ".cz"


static char* directory_path;

static DIR *dirp;
static struct dirent *file_info;




extern char output_name[256]; 
extern FILE *output_file;







#include "../include/files.h"


CZOPTIONS  header;


/*
 validates the path
  all paths must be finished by a slash

   e.g:   ./mydir/

   if is  ./mydir

   causes directory invalid  (return -1)

*/

void usage(){
    printf("HOW TO USE:\n");
    printf("cbfilegrabber <directory> <output_name>\n\n");
    printf("paths  must  finish with \'/\'\n\n");
    printf("e.g: cbfilegrabber ./test/ myoutput\n\n");
}


int validate_path(char *path){
	int path_size = strlen(path);
        char tmp[path_size + 1];
	int i = 0;


        memset(&tmp,0, path_size  + 1);
	strcpy(tmp, path);

        for(i = 0; i <= path_size;++i){
		switch(tmp[i]){
			case '\0':
				if(tmp[i - 1] == '/'){
                                        return 0;
				}

			break;
		}
	}
	

	return 1;
}




/*
	free all resources. must be called before exit()

*/

void free_resources(void){

	free(directory_path);
	free(file_info);

	if(output_file != NULL){
		fclose(output_file);
	}
}


void init_program(){
	printf(COLOR_YEL "cbfilegrabber ver %d.%d.%d\n\n", MAJOR_VER, MINOR_VER,BUILD_VER);
	printf(COLOR_YEL "by ryonagana?\n\n");
	printf(COLOR_RESET);
}



/*
	do a directory iteration 
	getting files information to write into a new output
*/

void process_files(char *directory){


	int file_qty = 0;


	if( (dirp = opendir(directory)) == NULL ){
		printf("Could not open %s directory or does not exists\n", directory);
		free_resources();
		exit(0);
	}


	strcpy(header.mgc, "&CZ");
	header.numfiles = file_qty;
	header.packed = 0;

	save_header(&header);


	while( (file_info = readdir(dirp)) != NULL){


		/* ignore if the dir is . or .. */
		if( !strcmp(file_info->d_name,".") ||  !strcmp(file_info->d_name,"..") ) continue;


		char path[256];

		/* append a new path with the file in the end */
		sprintf(path, "%s%s", directory, file_info->d_name);
		
	
		
		/* create a temporary FILE to read the file */
		FILE *read_file = fopen(path, "r");


		/*
			gets the name append to the path  if is successful read
			it stores  in the output file 
		*/
		if(save_file(read_file, file_info->d_name) > 0){
			printf("==============\n\n");
			
			/* a value how much files is  wrote in the file to edit the header later */
			++file_qty;
		}

		/* close teporary file resources
		 a bug can occur if an error occur  while  saving the file  the program cant read
		 this close and causes a little memory leak
		 */
		if(read_file != NULL) {
			fclose(read_file);
		}
		
		//printf("FILE: %s%s\n", directory, file_info->d_name);

	}


	/*
		after write all files you need to edit the header  and put how much files was written
		to iterate later 
	*/
	header.numfiles = file_qty;
	edit_header(&header);


	closedir(dirp);
	free(file_info);

	
}



/*
	process the arguments of the program
	and validate data before the program running 
*/

void process_args(int *p_argc, char *p_argv[]){
	
	char *p = p_argv[1];
	char *fname = p_argv[2];

	if(p == NULL){
                usage();
                //printf("\n\nDirectory cant be empty!\n\n");
		free_resources();
		exit(0);
	}





	directory_path =  mem_xmalloc(sizeof(char) * 64 + 1);
	strcpy(directory_path, p);


        if(validate_path(directory_path) != 0 ){

                printf(COLOR_RED "\n\nINVALID DIRECTORY!\n\n");
                printf(COLOR_RESET);
		free_resources();
		exit(0);
	}



	printf("READING DIRECTORY: %s\n", directory_path);


	//printf("VALID DIR? : %d", validate_path(directory_path));

	
	if(fname == NULL){
                //usage();
                printf("\n\nMust have a Output name!\n\n");
		free_resources();
		exit(0);
	}

	strcpy(output_name, fname);
	printf("OUTPUT FILE NAME: %s%s\n", output_name, EXTENSION );


}






int main(int argc, char** argv){
	init_program();
	process_args(&argc, argv);
	init_output();


	process_files(directory_path);





	free_resources();

	

	return 0;
}
