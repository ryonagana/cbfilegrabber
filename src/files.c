#include "../include/files.h"

FILE *output_file;
char output_name[256];

static int output_fd;


/*
Creates  a fresh header for the file!
*/
void save_header(CZOPTIONS *c){
	
	CZOPTIONS options;

	memcpy(&options, c, sizeof(CZOPTIONS));

	if(output_file != NULL){
		write(output_fd, &options, sizeof(CZOPTIONS));
	}


	

}


/*
	edit an existent header (MUST be written a fresh header)
*/

int edit_header(CZOPTIONS *options){

	CZOPTIONS c;

	memcpy(&c, options, sizeof(CZOPTIONS));


	if(output_file == NULL){
		return 0;
	}

	rewind(output_file);
	write(output_fd, &c, sizeof(CZOPTIONS));
}



/*
	starts the output file +  file decriptor
	one of main initializator
*/


void init_output(){

	if( strlen(output_name) == 0 ){
		fprintf(stderr, "\nOutput Name is Empty\n");
		exit(0);
	}

	char nuname[256];
	sprintf(nuname, "%s%c%s", output_name,'.',"cz");

	output_file = fopen(nuname, "w");
	
	if(output_file != NULL){
		output_fd = fileno(output_file);
	}


}



/*
	save a file into output, copying  chunks of bytes
*/

int save_file(FILE* f, char *filename){
	

	if(f == NULL){
		return 0;
	}


	char buf[CHUNK];
	int fd = fileno(f);
	size_t bytes_read = 0;
	size_t bytes_copied = 0;

	printf("Writing %s\n", filename);
	while( (bytes_read = fread(buf, CHUNK,1, f)) > 0 ){

		fwrite(buf, sizeof(char), sizeof(buf), output_file);
		bytes_copied += CHUNK;


	}


	printf("%s copied with success, %d bytes\n\n", filename, bytes_copied);
	return 1;

}



/*
	deinitialize  all file output to flush a new file 
*/
void close_output(){
	if(output_file != NULL){
		fclose(output_file);
	}

	close(output_fd);
}