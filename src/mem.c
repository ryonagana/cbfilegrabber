#include "../include/mem.h"


/*
 error when tries to alloc memory
*/
static void mem_alloc_error(void *p, const char *msg){
	//DEBUG_PRINT(msg);
	fprintf(stderr, msg);
	if (!p){
		free(p);
		p = NULL;
	}

	exit(1);
}

/*
	custom memory allocation
	tries to prone errors. still uses malloc()
*/
void* mem_xmalloc(size_t size){
	void* ptr = malloc(size);

	if (ptr == NULL){
		mem_alloc_error(ptr, "mem_alloc(): Error Tried to Alloc Memory\n\n");
	}

	return ptr;
}

/*
custom calloc
tries to prone errors. still uses calloc()
*/
void* mem_xcalloc(size_t mem, size_t sz){
	void *ptr = calloc(mem, sz);

	if (ptr == NULL){
		mem_alloc_error(ptr, "mem_calloc(): Error when trie to calloc\n\n");
	}

	return ptr;
}

/*
	extended free memory
*/
void mem_xfree(void** ptr){
	if (ptr != NULL){
		free(*ptr);
		*ptr = NULL;
	}


}
