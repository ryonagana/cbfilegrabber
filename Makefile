include src/Constants.mk

all:
	$(MAKE) -C src all
	mv src/$(BIN) .

clean:
	@$(MAKE) -C src clean