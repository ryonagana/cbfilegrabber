#ifndef  __MEM_HEADER__
#define   __MEM_HEADER__

#include <stdio.h>

void* mem_xmalloc(size_t size);
void* mem_xcalloc(size_t mem, size_t sz);
void mem_xfree(void** ptr);

#endif