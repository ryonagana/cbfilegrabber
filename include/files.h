#ifndef __FILES_HEADER__
#define __FILES_HEADER__


#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>


#define CHUNK 4096


struct _czfileoptions {
	char mgc[3];
	int numfiles;
	int packed;

};


typedef struct _czfileoptions  CZOPTIONS;



void save_header(CZOPTIONS *c);
int edit_header(CZOPTIONS *options);

int save_file(FILE* f, char *filename);
void init_output();
void close_output();

#endif